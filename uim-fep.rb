class UimFep < Formula
  desc "uim-fep"
  homepage "https://github.com/uim/uim"
  url "https://github.com/uim/uim/releases/download/uim-1.8.6/uim-1.8.6.tar.gz"
  sha256 "0eebcf5b2192d0cd11018adbd69e358bfa062d3b5d63c1dd0b73d5f63eb7afe7"

  depends_on "intltool"
  depends_on "gettext"
  depends_on "pkg-config" => :build

	patch :DATA

  fails_with :clang do
    cause "Segmentation fault in uim-module-manager"
  end

  def install
    system "./configure",
      "CFLAGS=-O0",
      "--prefix=#{prefix}",
    	"--without-gtk2",
    	"--without-gtk3",
    	"--disable-kde4-applet",
    	"--disable-notify",
    	"--without-qt4",
    	"--without-qt4-immodule",
    	"--without-x",
    	"--without-anthy-utf8",
    	"--with-skk"

		system "make", "install"
  end
end

__END__
--- a/uim/skk.c	2016-10-25 11:04:57.000000000 +0900
+++ b/uim/skk.c	2016-10-25 11:07:05.000000000 +0900
@@ -858,6 +859,7 @@
     while ((nr = read(skkservsock, &r, 1)) != -1 && nr != 0 && r != '\n')
       ;
     free(line);
+    skkserv_disconnected(di);
     return NULL;
   }
 }
