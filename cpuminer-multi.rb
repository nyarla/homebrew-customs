class CpuminerMulti < Formula
  desc "crypto cpuminer (linux + windows)"
  homepage "https://github.com/tpruvot/cpuminer-multi"
  url "https://github.com/tpruvot/cpuminer-multi/archive/v1.3-multi.tar.gz"
  sha256 "ec2bd234d25114781e42282c3ba05d0eea4c11133860d11da8999b6d2fb470e5"
  
  depends_on "automake"   => :build
  depends_on "autoconf"   => :build
  depends_on "pkg-config" => :build
  depends_on "curl"
  depends_on "openssl"

  def install
    system "./autogen.sh"
    system "./nomacro.pl"
    system "./configure", "--with-crypto", "--with-curl", "--prefix=#{prefix}"
    inreplace "algo/neoscrypt.c", "#if (WINDOWS)", "#define ASM 0\n#if (WINDOWS)"
    system "make", "install"
  end
end
